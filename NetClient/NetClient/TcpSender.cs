namespace NetClient;
using System.Net;
using System.Net.Sockets;
using System;
using System.Text;

public class TcpSender:IMsgSender, IDisposable
{
    private Socket sender;
    private bool disposed = false;
    public TcpSender(IPAddress ipAddr, IPEndPoint ipEndPoint)
    {
        sender = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        sender.Connect(ipEndPoint);
        ///???
        //Console.WriteLine("Strarting conection to {0} ", sender.RemoteEndPoint.ToString());
    }
    public int Send(string message)
    {
        byte[] msg = Encoding.UTF8.GetBytes(message);
        int bytesSent = sender.Send(msg);
        return bytesSent;
        //throw new System.NotImplementedException();
    }

    public string Receive()
    {
        byte[] bytes = new byte[1024];
        int bytesRec = sender.Receive(bytes);
        return Encoding.UTF8.GetString(bytes, 0, bytesRec);
        //throw new System.NotImplementedException();
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
    
    protected virtual void Dispose(bool disposing)
    {
        if (disposed)
        {
            return;
        }

        if (disposing)
        {
            // TODO: dispose managed state (managed objects).
        }
        sender.Close();
        disposed = true;
    }

    ~TcpSender()
    {
        Dispose(false);
    }
}